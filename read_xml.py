import xml.etree.ElementTree as ET

# import this data by reading from a file:
tree = ET.parse('country_data.xml')
root = tree.getroot()

# Or as a string:
# root = ET.fromstring(country_data_as_string)

# As an Element, root has a tag and a dictionary of attributes:
print(root.tag)   # 'data'

print(root.attrib)  # {}

# It also has children nodes over which we can iterate:

for child in root:
    print(child.tag, child.attrib)

# Output
# country {'name': 'Liechtenstein'}
# country {'name': 'Singapore'}
# country {'name': 'Panama'}
